# README

Blog API.

## Implemented actions

**POST  /posts  params: { login, title, content }** - create a new post

**PATCH /posts/:id/rate params: { rating }** - set rating to post

**GET    /posts/top/:n** - get top rated posts

**GET    /suspicious_authors** - get list of IP from which posted several authors

## Used versions

* ruby 2.6.4
* rails 6.0.0
* psql (PostgreSQL) 10.10

## Setup

* Install dependencies: `bundle install`
* Prepare pg user with login `$BLOG_API_DB_USER` and password `$BLOG_API_DB_PASSWORD`
* Create database:
```bash
rails db:create
rails db:migrate
```
* Seed data for devopment:
```bash
rails db:seed
```

## Run unit tests

```
bundle exec rspec
```

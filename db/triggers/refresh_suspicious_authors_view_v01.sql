CREATE TRIGGER refresh_suspicious_authors_view
AFTER INSERT ON authors
FOR EACH STATEMENT
EXECUTE PROCEDURE refresh_suspicious_authors_view();

require 'faker'
require 'progress_bar'

def generate_ips(count)
  ips = []
  count.times { ips << Faker::Internet.unique.ip_v4_address }
  ips
end

def generate_users(count)
  user_logins = []
  (1..count).each do
    login = Faker::Name.unique.first_name
    user_logins << login
    Users::Create.call(login)
  end
  user_logins
end

def generate_posts(count, ip_count, authors, bar)
  ips = generate_ips(ip_count)
  (1..count).each do |i|
    params = {
      login: authors.sample,
      title: "title_#{i}",
      content: Faker::Lorem.paragraph
    }
    Posts::Create.call(params, ips.sample)
    bar.increment!
  end
end

def generate_ratings(count, posts_count)
  bar = ProgressBar.new(count)
  count.times do
    params = {
      id: rand(1..posts_count),
      rating: rand(1..5)
    }
    Posts::Rate.call(params)
    bar.increment!
  end
end

if Rails.env == 'development'
  POSTS_COUNT = 200_000
  RATINGS_COUNT = 1000

  p 'Posts creating:'
  bar = ProgressBar.new(POSTS_COUNT)
  authors = generate_users(100)
  generate_posts(1000, 5, authors[0..9], bar)
  generate_posts(99_000, 20, authors[10..49], bar)
  generate_posts(100_000, 25, authors[50..99], bar)

  p 'Populate rating:'
  generate_ratings(RATINGS_COUNT, POSTS_COUNT)
end

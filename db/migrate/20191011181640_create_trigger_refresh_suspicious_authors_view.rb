class CreateTriggerRefreshSuspiciousAuthorsView < ActiveRecord::Migration[5.0]
  def change
    create_trigger :refresh_suspicious_authors_view, on: :authors
  end
end

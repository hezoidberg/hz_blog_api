class CreateAuthors < ActiveRecord::Migration[6.0]
  def change
    create_table :authors do |t|
      t.references :ip_address
      t.references :user

      t.timestamps
    end

    add_index :authors, %i[ip_address_id user_id], unique: true
  end
end

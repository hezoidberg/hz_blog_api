class AddIndexToPostsOnAvgRating < ActiveRecord::Migration[6.0]
  def change
    add_index :posts,
              'avg_rating(posts)',
              name: 'index_on_avg_rating'
  end
end

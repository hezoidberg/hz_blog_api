class CreatePosts < ActiveRecord::Migration[6.0]
  def change
    create_table :posts do |t|
      t.references :author
      t.string :title, null: false
      t.text :content, null: false
      t.integer :ratings_count, default: 0
      t.integer :ratings_sum, default: 0

      t.timestamps
    end
  end
end

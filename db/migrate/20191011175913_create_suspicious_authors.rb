class CreateSuspiciousAuthors < ActiveRecord::Migration[6.0]
  def change
    create_view :suspicious_authors, materialized: true
  end
end

class CreatePostRatings < ActiveRecord::Migration[6.0]
  def change
    create_table :post_ratings do |t|
      t.references :post
      t.references :user
      t.integer :rating, limit: 1

      t.timestamps
    end
  end
end

CREATE OR REPLACE FUNCTION avg_rating(posts) RETURNS float AS $$
    SELECT coalesce(cast($1.ratings_sum AS float) / nullif($1.ratings_count, 0), 0.0);
$$ LANGUAGE SQL;

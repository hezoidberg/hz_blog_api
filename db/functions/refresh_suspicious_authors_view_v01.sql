CREATE OR REPLACE FUNCTION refresh_suspicious_authors_view() RETURNS trigger AS $function$
BEGIN
  REFRESH MATERIALIZED VIEW suspicious_authors;
  RETURN NULL;
END;
$function$ LANGUAGE plpgsql;

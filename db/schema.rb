# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_10_09_073330) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "post_ratings", force: :cascade do |t|
    t.bigint "post_id"
    t.bigint "user_id"
    t.integer "rating", limit: 2
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["post_id"], name: "index_post_ratings_on_post_id"
    t.index ["user_id"], name: "index_post_ratings_on_user_id"
  end

  create_table "posts", force: :cascade do |t|
    t.bigint "user_id"
    t.string "title", null: false
    t.text "content", null: false
    t.string "author_ip"
    t.integer "ratings_count", default: 0
    t.integer "ratings_sum", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index "avg_rating(posts.*)", name: "index_on_avg_rating"
    t.index ["user_id"], name: "index_posts_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "login", limit: 50, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["login"], name: "index_users_on_login", unique: true
  end


  create_function :avg_rating, sql_definition: <<-SQL
      CREATE OR REPLACE FUNCTION public.avg_rating(posts)
       RETURNS double precision
       LANGUAGE sql
      AS $function$
          SELECT coalesce(cast($1.ratings_sum AS float) / nullif($1.ratings_count, 0), 0.0);
      $function$
  SQL
end

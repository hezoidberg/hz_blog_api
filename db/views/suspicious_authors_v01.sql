SELECT i.value AS ip, array_agg(u.login) AS logins
FROM authors a
INNER JOIN users u ON u.id = a.user_id
INNER JOIN ip_addresses i ON i.id = a.ip_address_id
GROUP BY i.value HAVING (count(u.login) > 1);

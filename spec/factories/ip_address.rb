FactoryBot.define do
  factory :ip_address do
    value { Faker::Internet.ip_v4_address }
  end
end

FactoryBot.define do
  factory :author do
    user { build(:user) }
    ip_address { build(:ip_address) }
  end
end

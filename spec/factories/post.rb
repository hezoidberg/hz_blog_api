FactoryBot.define do
  factory :post do
    author { build(:author) }
    ratings_sum { 9 }
    ratings_count { 2 }
    title { 'ololo' }
    content { 'trololo' }
  end
end

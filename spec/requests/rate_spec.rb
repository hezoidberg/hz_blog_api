require 'rails_helper'

describe 'Rate', type: :request do
  let(:post) { create(:post) }

  context 'when successful' do
    it 'set rating to post' do
      allow(Post).to receive(:find_by_id).with(1).and_return(post)
      patch '/posts/1/rate', params: { rating: 5 }

      expect(response).to have_http_status(:ok)
      expect(JSON.parse(response.body)).to eq('rating' => 4.6667)
    end
  end

  context 'when unsuccessful' do
    it 'set invalid rating' do
      patch '/posts/1/rate', params: { rating: 6 }
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end
end

require 'rails_helper'

describe 'Get top', type: :request do
  context 'when successful' do
    before do
      count = 1
      5.times do
        create(:post, title: "N#{count}", ratings_sum: 5, ratings_count: count)
        count += 1
      end
    end

    it 'returns sorted by avg_rating posts' do
      get '/posts/top/3'
      expect(response).to have_http_status(:ok)

      top_list = JSON.parse(response.body)['top']
      expect(top_list.count).to eq(3)
      expect(top_list.map { |x| x['title'] }).to eq(%w[N1 N2 N3])
    end
  end

  context 'when unsuccessful' do
    it 'exceeds limit' do
      get '/posts/top/3000'
      expect(response).to have_http_status(:not_found)
    end

    it 'get negative limit' do
      get '/posts/top/-4'
      expect(response).to have_http_status(:not_found)
    end
  end
end

require 'rails_helper'

describe 'Get author`s logins grouped by ip', type: :request do
  context 'when successful' do
    before do
      ip1 = create(:ip_address, value: '192.168.1.2')
      ip2 = create(:ip_address, value: '192.168.1.3')
      author1 = create(:author, ip_address: ip1)
      author2 = create(:author, ip_address: ip2)
      author3 = create(:author, ip_address: ip2)

      create(:post, title: 'post1', author: author1)
      create(:post, title: 'post2', author: author2)
      create(:post, title: 'post3', author: author3)
    end

    it 'returns ok status' do
      get '/suspicious_authors'
      expect(response).to have_http_status(:ok)
    end

    it 'returns grouped by ip logins' do
      get '/suspicious_authors'

      authors = JSON.parse(response.body)
      expect(authors.count).to eq(1)
      expect(authors.first['logins'].count).to eq(2)
      expect(authors.first['ip']).to eq('192.168.1.3')
    end

    context 'when update view' do
      before do
        ip = IpAddress.find_by(value: '192.168.1.3')
        author4 = create(:author, ip_address: ip)
        create(:post, title: 'post4', author: author4)
      end

      it 'updates suspicious_authors view' do
        get '/suspicious_authors'

        authors = JSON.parse(response.body)
        expect(authors.first['logins'].count).to eq(3)
      end
    end
  end
end

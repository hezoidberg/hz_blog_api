require 'rails_helper'

describe 'Post', type: :request do
  let(:request) { instance_double(ActionDispatch::Request) }
  let(:user) { instance_double('User', login: 'Ivan') }
  let(:title1) { 'title 1' }
  let(:content1) { 'ololo trololo' }

  context 'when successful' do
    it 'creates a new post' do
      allow(request).to receive(:remote_ip).and_return('192.168.99.225')
      post '/posts', params: { login: user.login, title: title1, content: content1 }

      expect(response).to have_http_status(:ok)
      expect(response.content_type).to eq 'application/json; charset=utf-8'
    end
  end

  context 'when unsuccessful' do
    it 'has no title' do
      post '/posts', params: { login: user.login, content: content1 }

      expect(response).to have_http_status(:unprocessable_entity)
      expect(JSON.parse(response.body)['errors']).to eq(["Title can't be blank"])
    end

    it 'has empty content' do
      post '/posts', params: { login: user.login, title: title1, content: '' }

      expect(response).to have_http_status(:unprocessable_entity)
      expect(JSON.parse(response.body)['errors']).to eq(["Content can't be blank"])
    end
  end
end

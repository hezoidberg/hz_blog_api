require 'rails_helper'

describe Posts::Rate do
  describe '.call' do
    let(:params) do
      {
        id: 1,
        rating: 3
      }
    end
    let(:post) { create(:post) }

    it 'rates post' do
      allow(Post).to receive(:find_by_id).with(1).and_return(post)
      post_rate = described_class.call(params)

      expect(post_rate.success?).to be true
      expect(post_rate.object).to eq(4.0)
    end
  end
end

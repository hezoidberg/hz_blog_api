require 'rails_helper'

describe Posts::Create do
  describe '.call' do
    let(:params) do
      {
        login: 'test',
        title: 'title',
        content: 'content'
      }
    end
    let(:author_ip) { '192.168.1.3' }

    it 'saves post' do
      ip_address = create(:ip_address, value: author_ip)
      post_create = described_class.call(params, author_ip)
      expect(post_create.success?).to be true
      expect(post_create.object.author.ip_address_id).to eq(ip_address.id)
    end
  end
end

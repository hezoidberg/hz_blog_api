require 'rails_helper'

describe Users::Create do
  describe '.call' do
    let(:login) { 'Vasia' }

    it 'saves user' do
      response = described_class.call(login)
      expect(response.user.login).to eq(login)
    end

    it 'raises ActiveRecord::RecordNotUnique error' do
      described_class.call(login)
      expect { described_class.call(login) }.to raise_error(ActiveRecord::RecordNotUnique)
    end
  end
end

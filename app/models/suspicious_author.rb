class SuspiciousAuthor < ApplicationRecord
  def self.refresh
    Scenic.database.refresh_materialized_view('suspicious_authors', concurrently: true)
  end

  def readonly?
    true
  end
end

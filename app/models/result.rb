class Result
  def self.fail(errors)
    new(false, nil, errors)
  end

  def self.success(object)
    new(true, object)
  end

  attr_reader :object, :errors

  def initialize(success = nil, object = nil, errors = [])
    @success = success
    @object = object
    @errors = errors
  end

  def success?
    @success
  end
end

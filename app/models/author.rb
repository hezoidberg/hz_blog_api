class Author < ApplicationRecord
  has_many :posts
  belongs_to :ip_address
  belongs_to :user
end

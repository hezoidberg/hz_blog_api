class PostsController < ApplicationController
  def create
    post_handler = Posts::Create.call(params, request.remote_ip)
    if post_handler.success?
      render json: post_handler.object, status: :ok
    else
      render json: { errors: post_handler.errors }, status: :unprocessable_entity
    end
  end

  def show
    post = Post.find_by_id(params[:id])
    if post
      render json: post, status: :ok
    else
      render json: { error: "Couldn't find Post with 'id'=#{params[:id]}" }, status: :not_found unless post
    end
  end

  def rate
    rate = Posts::Rate.call(params)
    if rate.success?
      render json: { rating: rate.object }, status: :ok
    else
      render json: { errors: rate.errors }, status: :unprocessable_entity
    end
  end

  def top
    avg_ratings = Posts::GetTop.call(params[:n])
    if avg_ratings.success?
      render json: { top: avg_ratings.object }, status: :ok
    else
      render json: { errors: avg_ratings.errors }, status: :not_found
    end
  end

  def suspicious_authors
    render json: Posts::GetSuspiciousAuthors.call, status: :ok
  end
end

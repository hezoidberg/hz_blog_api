class ApplicationService
  include ActiveModel::Validations

  def self.call(*args, &block)
    new(*args, &block).call
  end

  def find_or_create(model, **args)
    model.find_or_create_by(args)
  rescue ActiveRecord::RecordNotUnique
    retry
  end
end

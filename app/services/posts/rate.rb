module Posts
  class Rate < ApplicationService
    attr_reader :post_id, :rating

    validates_presence_of :post_id, :rating
    validates_inclusion_of :rating, in: 1..5

    def initialize(params)
      @post_id = params[:id].to_i
      @rating = params[:rating].to_i
    end

    def call
      return Result.fail(errors.full_messages) unless valid?

      post = Post.find_by_id(post_id)
      if post
        Post.where(id: post_id)
            .update_all(['ratings_count = ratings_count + 1, ratings_sum = ratings_sum + ?', rating])
        return Result.success(caclute_rating(post))
      end
      Result.fail("Couldn't find Post with id=#{post_id}")
    end

    private

    def caclute_rating(post)
      ((post.ratings_sum + rating) / (post.ratings_count + 1.0)).round(4)
    end
  end
end

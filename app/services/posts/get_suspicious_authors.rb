module Posts
  class GetSuspiciousAuthors < ApplicationService
    def call
      SuspiciousAuthor.all
    end
  end
end

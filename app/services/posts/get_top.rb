module Posts
  class GetTop < ApplicationService
    attr_reader :n

    validates_inclusion_of :n, in: 1..1000

    def initialize(top_n)
      @n = top_n.to_i
    end

    def call
      return Result.fail(errors.full_messages) unless valid?

      top_posts = Post.order('posts.avg_rating DESC')
                      .limit(n)
                      .pluck(:title, :content)
                      .map { |e| { title: e.first, content: e.last } }
      Result.success(top_posts)
    end
  end
end

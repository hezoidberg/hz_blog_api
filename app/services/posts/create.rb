module Posts
  class Create < ApplicationService
    attr_reader :login, :title, :content, :author_ip

    validates_presence_of :login, :title, :content

    def initialize(params, author_ip)
      @login = params[:login]
      @title = params[:title]
      @content = params[:content]
      @author_ip = author_ip
    end

    def call
      return Result.fail(errors.full_messages) unless valid?

      post = Post.create(author_id: prepare_author.id, title: title, content: content)
      Result.success(post)
    end

    def prepare_author
      user = find_or_create(User, login: login)
      ip = find_or_create(IpAddress, value: author_ip)

      find_or_create(Author, user_id: user.id, ip_address_id: ip.id)
    end
  end
end

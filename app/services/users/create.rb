module Users
  class Create < ApplicationService
    attr_reader :login

    validates_presence_of :login

    def initialize(login)
      @login = login
    end

    def call
      return OpenStruct.new(success?: false, user: nil, errors: errors.full_messages) unless valid?

      user = User.create(login: login)
      OpenStruct.new(success?: true, user: user, errors: nil)
    end
  end
end

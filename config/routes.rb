Rails.application.routes.draw do
  resources :posts, only: [:show, :create]

  patch '/posts/:id/rate', to: 'posts#rate'
  get '/posts/top/:n', to: 'posts#top'
  get '/suspicious_authors', to: 'posts#suspicious_authors'
end
